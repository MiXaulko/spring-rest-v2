alter table ConsumerCompany
  add column id integer;
alter table CompanyProductType
add column id integer;

create or replace function companyproducttypeFunction() returns trigger as
$ConsumerCompanyFunction$
declare nextVal integer;
begin
  nextVal = nextval('idgenerator');
  NEW.id:=nextVal;
  return NEW;
end;

$ConsumerCompanyFunction$ LANGUAGE plpgsql;

create trigger companyproducttypeTrigger
  before insert on companyproducttype for each row execute procedure companyproducttypeFunction();

----------
create or replace function consumercompanyFunction() returns trigger as
$ConsumerCompanyFunction$
declare nextVal integer;
begin
  nextVal = nextval('idgenerator');
  NEW.id:=nextVal;
  return NEW;
end;

$ConsumerCompanyFunction$ LANGUAGE plpgsql;

create trigger consumercompanyTrigger
  before insert on consumercompany for each row execute procedure consumercompanyFunction();
