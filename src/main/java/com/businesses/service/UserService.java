package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.entity.UserDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

import static java.util.Collections.emptyList;

@Service
public class UserService implements UserDetailsService {
    private DAOManager<UserDb, Integer> manager;

    private PasswordEncoder encoder;

    @Autowired
    @Qualifier(value = "userDAOManager")
    public void setManager(DAOManager<UserDb, Integer> manager) {
        this.manager = manager;
    }

    @Autowired
    public void setEncoder(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDb userDbVO;
        try {
            userDbVO = manager.findByString("login", username);
        } catch (NoResultException e) {
            throw new UsernameNotFoundException(username);
        }
        return new User(userDbVO.getLogin(), userDbVO.getPassword(), emptyList());
    }

    @Transactional
    public void register(UserDb userDb) {
        userDb.setPassword(encoder.encode(userDb.getPassword()));
        manager.create(userDb);
    }
}
