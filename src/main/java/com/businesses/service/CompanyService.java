package com.businesses.service;

import com.businesses.dao.CompanyRepository;
import com.businesses.dao.DAOManager;
import com.businesses.dto.CompanyDTO;
import com.businesses.dto.UpdateCompanyDTO;
import com.businesses.entity.*;
import com.businesses.utils.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.businesses.service.Constants.*;

@Service
public class CompanyService {
    private CompanyRepository companyDao;
    private DAOManager<CompanyWorker, Integer> companyWorkerDao;
    private DAOManager<CompanyProductType, Integer> companyProductTypeDao;
    private DAOManager<CompanyArea, Integer> companyAreaDao;
    private DAOManager<ConsumerCompany, Integer> consumerCompanyDao;
    private DAOManager<AreaType, Integer> areaTypeDao;
    private DAOManager<ProductType, Integer> productTypeDao;
    private DAOManager<WorkerType, Integer> workerTypeDao;
    private DAOManager<OwnershipType, Integer> ownershipTypeDao;
    private DAOManager<IndustryType, Integer> industryTypeDao;
    private DAOManager<Partner, Integer> partnerDao;
    private SerializerAdapter serializer;

    @Autowired
    public CompanyService(CompanyRepository companyDao,
                          @Qualifier(value = "companyWorkerDAOManager") DAOManager<CompanyWorker, Integer> companyWorkerDao,
                          @Qualifier(value = "companyProductTypeDAOManager") DAOManager<CompanyProductType, Integer> companyProductTypeDao,
                          @Qualifier(value = "companyAreaDAOManager") DAOManager<CompanyArea, Integer> companyAreaDao,
                          @Qualifier(value = "consumerCompanyDAOManager") DAOManager<ConsumerCompany, Integer> consumerCompanyDao,
                          @Qualifier(value = "areaTypeDAOManager") DAOManager<AreaType, Integer> areaTypeDao,
                          @Qualifier(value = "productTypeDAOManager") DAOManager<ProductType, Integer> productTypeDao,
                          @Qualifier(value = "workerTypeDAOManager") DAOManager<WorkerType, Integer> workerTypeDao,
                          @Qualifier(value = "ownershipTypeDAOManager") DAOManager<OwnershipType, Integer> ownershipTypeDao,
                          @Qualifier(value = "industryTypeDAOManager") DAOManager<IndustryType, Integer> industryTypeDao,
                          @Qualifier(value = "partnerDAOManager") DAOManager<Partner, Integer> partnerDao,
                          SerializerAdapter serializer) {
        this.companyDao = companyDao;
        this.companyWorkerDao = companyWorkerDao;
        this.companyProductTypeDao = companyProductTypeDao;
        this.companyAreaDao = companyAreaDao;
        this.consumerCompanyDao = consumerCompanyDao;
        this.areaTypeDao = areaTypeDao;
        this.productTypeDao = productTypeDao;
        this.workerTypeDao = workerTypeDao;
        this.ownershipTypeDao = ownershipTypeDao;
        this.industryTypeDao = industryTypeDao;
        this.partnerDao = partnerDao;
        this.serializer = serializer;
    }

    public String get() {
        //написать native запрос
        return serializer.getJSON(CompanyDTO.getListCompanyDTO(companyDao.findAll()));
    }

    @Transactional
    public void delete(UpdateCompanyDTO dto) {
        String type = dto.getType();
        Company found = companyDao.getById(dto.getId());
        Preconditions.checkNotNull(found);
        switch (type) {
            case DEFAULT:
                companyDao.delete(found);
                break;
            case PROVIDER:
                Integer providerID = dto.getPartnerId();
                Partner provider = partnerDao.get(providerID);
                Preconditions.checkNotNull(provider);
                provider.getCompanySet().removeIf(e -> e.getProviderSet().contains(provider));
                break;
            case CONSUMER:
                Integer consumerID = dto.getPartnerId();
                Integer productTypeId = dto.getProducttypeId();
                provider = partnerDao.get(consumerID);
                Preconditions.checkNotNull(provider);
                Optional<ConsumerCompany> c = provider.getConsumerCompanySet().stream().
                        filter(e -> e.getCompany().getId().equals(found.getId()) && e.getProductType().getId().equals(productTypeId)).
                        findFirst();
                c.ifPresent(e -> {
                    found.getConsumerCompanySet().remove(e);
                    provider.getConsumerCompanySet().remove(e);
                    consumerCompanyDao.delete(e);
                });
                break;
            case AREATYPE:
                Integer areatypeId = dto.getAreatypeId();
                Optional<CompanyArea> ca = found.getCompanyAreaSet().stream().
                        filter(e -> e.getAreaType().getId().equals(areatypeId)).
                        findFirst();
                ca.ifPresent(e -> {
                    found.getCompanyAreaSet().remove(e);
                    companyAreaDao.delete(e);
                });
                break;
            case WORKERTYPE:
                Integer workertypeId = dto.getWorkertypeId();
                Optional<CompanyWorker> cw = found.getCompanyWorkerSet().stream().
                        filter(e -> e.getWorkerType().getId().equals(workertypeId)).
                        findFirst();
                cw.ifPresent(e -> {
                    found.getCompanyWorkerSet().remove(e);
                    companyWorkerDao.delete(e);
                });
                break;
            case PRODUCTTYPE:
                Integer producttypeId = dto.getProducttypeId();
                Optional<CompanyProductType> cpt = found.getCompanyProductTypeSet().stream().
                        filter(e -> e.getProductType().getId().equals(producttypeId)).
                        findFirst();
                cpt.ifPresent(e -> {
                    found.getCompanyProductTypeSet().remove(e);
                    companyProductTypeDao.delete(e);
                });
                break;
        }
    }

    @Transactional
    public String update(UpdateCompanyDTO dto) {
        String type = dto.getType();
        Company after = companyDao.getById(dto.getId());
        Preconditions.checkNotNull(after);
        switch (type) {
            case DEFAULT:
                after.setName(dto.getName());
                after.setAddress(dto.getAddress());
                after.setPhone(dto.getPhone());
                after.setFax(dto.getFax());
                OwnershipType ownershipType = ownershipTypeDao.get(dto.getOwnershipTypeId());
                Preconditions.checkNotNull(ownershipType);
                after.setOwnershipType(ownershipType);
                IndustryType industryType = industryTypeDao.get(dto.getIndustryTypeId());
                Preconditions.checkNotNull(industryType);
                after.setIndustryType(industryType);
                break;
            case PROVIDER:
            case CONSUMER:
                //todo not editable, need hide button on front
                break;
            case AREATYPE:
                Integer areatypeId = dto.getAreatypeId();
                Integer value = dto.getValue();
                after.getCompanyAreaSet().stream().
                        filter(e -> e.getAreaType().getId().equals(areatypeId)).
                        findFirst().
                        ifPresent(e -> e.setValue(value));
                break;
            case WORKERTYPE:
                Integer workertypeId = dto.getWorkertypeId();
                value = dto.getValue();
                after.getCompanyWorkerSet().stream().
                        filter(e -> e.getWorkerType().getId().equals(workertypeId)).
                        findFirst().
                        ifPresent(e -> e.setValue(value));
                break;
            case PRODUCTTYPE:
                Integer producttypeId = dto.getProducttypeId();
                value = dto.getValue();
                after.getCompanyProductTypeSet().stream().
                        filter(e -> e.getProductType().getId().equals(producttypeId)).
                        findFirst().
                        ifPresent(e -> e.setValue(value));
                break;
        }
        return serializer.getJSON(new CompanyDTO(after));
    }

    @Transactional
    public String create(UpdateCompanyDTO companyDTO) {
        String type = companyDTO.getType();
        Company after = type.equals(DEFAULT) ? companyDTO.createCompany() : companyDao.getById(companyDTO.getId());
        Preconditions.checkNotNull(after);
        switch (type) {
            case DEFAULT:
                Integer ownershipTypeId = companyDTO.getOwnershipTypeId();
                Integer industryTypeId = companyDTO.getIndustryTypeId();
                OwnershipType ownershipType = ownershipTypeDao.get(ownershipTypeId);
                Preconditions.checkNotNull(ownershipType);
                after.setOwnershipType(ownershipType);
                IndustryType industryType = industryTypeDao.get(industryTypeId);
                Preconditions.checkNotNull(industryType);
                after.setIndustryType(industryType);
                companyDao.save(after);
                break;
            case PROVIDER:
                Integer providerId = companyDTO.getPartnerId();
                Partner provider = partnerDao.get(providerId);
                Preconditions.checkNotNull(provider);
                if (provider.getCompanySet().stream().anyMatch(e -> e.equals(after))) {
                    throw new IllegalArgumentException(ALREADY_EXIST);
                }
                provider.getCompanySet().add(after);
                after.getProviderSet().add(provider);
                break;
            case CONSUMER:
                Integer consumerId = companyDTO.getPartnerId();
                Partner consumer = partnerDao.get(consumerId);
                Preconditions.checkNotNull(consumer);
                Integer productTypeId = companyDTO.getProducttypeId();
                ProductType productType = productTypeDao.get(productTypeId);
                Preconditions.checkNotNull(productType);
                boolean alreadyExist = after.getConsumerCompanySet().stream().
                        anyMatch(e -> e.getProductType().equals(productType) && e.getConsumer().equals(consumer));
                if (alreadyExist) {
                    throw new IllegalArgumentException(ALREADY_EXIST);
                }
                ConsumerCompany consumerCompany = new ConsumerCompany(consumer, after, productType);
                consumerCompanyDao.create(consumerCompany);
                after.getConsumerCompanySet().add(consumerCompany);
                break;
            case AREATYPE:
                Integer areatypeId = companyDTO.getAreatypeId();
                AreaType areaType = areaTypeDao.get(areatypeId);
                Preconditions.checkNotNull(areaType);
                alreadyExist = after.getCompanyAreaSet().stream().
                        anyMatch(e -> e.getAreaType().equals(areaType));
                if (alreadyExist) {
                    throw new IllegalArgumentException(ALREADY_EXIST);
                }
                Integer value = companyDTO.getValue();
                after.getCompanyAreaSet().add(new CompanyArea(after, areaType, value));
                break;
            case WORKERTYPE:
                Integer workerTypeId = companyDTO.getWorkertypeId();
                WorkerType workerType = workerTypeDao.get(workerTypeId);
                Preconditions.checkNotNull(workerType);
                alreadyExist = after.getCompanyWorkerSet().stream().
                        anyMatch(e -> e.getWorkerType().equals(workerType));
                if (alreadyExist) {
                    throw new IllegalArgumentException(ALREADY_EXIST);
                }
                value = companyDTO.getValue();
                after.getCompanyWorkerSet().add(new CompanyWorker(after, workerType, value));
                break;
            case PRODUCTTYPE:
                Integer producttypeId = companyDTO.getProducttypeId();
                productType = productTypeDao.get(producttypeId);
                Preconditions.checkNotNull(productType);
                alreadyExist = after.getCompanyProductTypeSet().stream().
                        anyMatch(e -> e.getProductType().equals(productType) && e.getMonthR().equals(companyDTO.getMonth()) && e.getYearR().equals(companyDTO.getYear()));
                if (alreadyExist) {
                    throw new IllegalArgumentException(ALREADY_EXIST);
                }
                value = companyDTO.getValue();
                Integer month = companyDTO.getMonth();
                Integer year = companyDTO.getYear();
                after.getCompanyProductTypeSet().add(new CompanyProductType(after, productType, year, month, value));
                break;
        }
        return serializer.getJSON(new CompanyDTO(after));
    }
}