package com.businesses.service;

import com.businesses.exceptions.EntityNotExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.businesses.service.Constants.*;

@ControllerAdvice
public class ExceptionInterceptor {

    private SerializerAdapter serializer;

    @Autowired
    public ExceptionInterceptor(SerializerAdapter serializer) {
        this.serializer = serializer;
    }


    @ExceptionHandler(Throwable.class)
    public ResponseEntity<String> throwableInterceptor(Throwable ex) {
        String bodyOfResponse = "Something wrong, stacktrace: \n" +
                Arrays.stream(ex.getStackTrace())
                        .map(StackTraceElement::toString)
                        .collect(Collectors.joining("\n"));
        return new ResponseEntity<>(bodyOfResponse, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityNotExistsException.class)
    public ResponseEntity<String> doesNotExistException(EntityNotExistsException ex) {
        return new ResponseEntity<>(serializer.getJSON(NOT_EXIST), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DataIntegrityViolationException.class, IllegalArgumentException.class})
    public ResponseEntity<String> alreadyExistException(DataIntegrityViolationException ex) {
        return new ResponseEntity<>(serializer.getJSON(ALREADY_EXIST), HttpStatus.BAD_REQUEST);
    }

    //todo добавить обработку исключения, когда не можем удалить справочник, тк он используется в связанных таблицах
}
