package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.dto.DictionaryDTO;
import com.businesses.entity.OwnershipType;
import com.businesses.exceptions.EntityNotExistsException;
import com.businesses.utils.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OwnershipTypeService implements AbstractService<OwnershipType, Integer> {

    private DAOManager<OwnershipType, Integer> daoManager;

    private SerializerAdapter serializer;

    @Autowired
    public OwnershipTypeService(@Qualifier(value = "ownershipTypeDAOManager") DAOManager<OwnershipType, Integer> daoManager,
                                SerializerAdapter serializer) {
        this.daoManager = daoManager;
        this.serializer = serializer;
    }

    @Transactional
    @CacheEvict(value = "ownershipTypeCache", allEntries = true)
    public String create(OwnershipType ownershipType) {
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.create(ownershipType)));
    }

    @Cacheable(value = "ownershipTypeCache")
    public String get() {
        return serializer.getJSON(DictionaryDTO.getListDictionaryDTO(daoManager.getAll()));
    }

    @Transactional
    @CacheEvict(value = "ownershipTypeCache", allEntries = true)
    public void delete(Integer id) {
        OwnershipType found = daoManager.get(id);
        Preconditions.checkNotNull(found);
        daoManager.delete(found);
    }

    @Transactional
    @CacheEvict(value = "ownershipTypeCache", allEntries = true)
    public String update(OwnershipType ownershipType) {
        OwnershipType found = daoManager.get(ownershipType.getId());
        Preconditions.checkNotNull(found);
        found.updateName(ownershipType.getName());
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.update(found)));
    }
}
