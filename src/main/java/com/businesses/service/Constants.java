package com.businesses.service;

public class Constants
{
    public static final String ALREADY_EXIST = "Данная запись уже существует";
    public static final String NOT_EXIST = "Данная запись не существует";
    public static final String CANNOT_BE_DELETED = "Данную запись нельзя удалить, тк существуют записи, использующие ее";
    public static final String TOKEN_OUTDATED = "Ваша сессия устарела, необходимо войти в систему заново";
    public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";
    public static final String DEFAULT = "default";
    public static final String PROVIDER = "provider";
    public static final String CONSUMER = "consumer";
    public static final String AREATYPE = "areatype";
    public static final String WORKERTYPE = "workertype";
    public static final String PRODUCTTYPE = "producttype";
}
