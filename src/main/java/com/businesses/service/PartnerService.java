package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.dto.DictionaryDTO;
import com.businesses.dto.PartnerDTO;
import com.businesses.entity.ExternalType;
import com.businesses.entity.ExternalTypeName;
import com.businesses.entity.Partner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

import static com.businesses.utils.Preconditions.checkNotNull;

@Service
public class PartnerService implements AbstractService<PartnerDTO, Integer> {

    private DAOManager<Partner, Integer> partnerDAOManager;

    private DAOManager<ExternalType, Integer> externalTypeDAOManager;

    private DAOManager<ExternalTypeName, Integer> externalTypeNameDAOManager;

    private SerializerAdapter serializer;

    @Autowired
    public PartnerService(@Qualifier(value = "partnerDAOManager") DAOManager<Partner, Integer> partnerDAOManager,
                          @Qualifier(value = "externalTypeDAOManager") DAOManager<ExternalType, Integer> externalTypeDAOManager,
                          @Qualifier(value = "externalTypeNameDAOManager") DAOManager<ExternalTypeName, Integer> externalTypeNameDAOManager,
                          SerializerAdapter serializer) {
        this.partnerDAOManager = partnerDAOManager;
        this.externalTypeDAOManager = externalTypeDAOManager;
        this.externalTypeNameDAOManager = externalTypeNameDAOManager;
        this.serializer = serializer;
    }

    @Transactional
    @CacheEvict(value = "partnerCache", allEntries = true)
    public String create(PartnerDTO partnerDTO) {
        Partner createdPartner = new Partner(partnerDTO.getName());
        createdPartner = partnerDAOManager.create(createdPartner);
        if (partnerDTO.getIsExternal() != null && partnerDTO.getIsExternal()) {
            String nameLocation = partnerDTO.getNameLocation();
            //todo validation null
            ExternalTypeName externalTypeName = getExternalTypeByName(nameLocation);

            ExternalType externalType = new ExternalType(createdPartner.getId(), externalTypeName);
            externalTypeDAOManager.create(externalType);
            createdPartner.setExternalType(externalType);
        }

        PartnerDTO dto = new PartnerDTO(createdPartner);
        return serializer.getJSON(dto);
    }

    @Cacheable(value = "partnerCache")
    public String get() {
        return serializer.getJSON(PartnerDTO.getListPartnerDTO(partnerDAOManager.getAll()));
    }

    @Transactional
    @CacheEvict(value = "partnerCache", allEntries = true)
    public void delete(Integer id) {
        Partner found = partnerDAOManager.get(id);
        checkNotNull(found);
        partnerDAOManager.delete(found);
    }

    @Transactional
    @CacheEvict(value = "partnerCache", allEntries = true)
    public String update(PartnerDTO partnerDTO) {
        Partner result = partnerDAOManager.get(partnerDTO.getId());
        checkNotNull(result);
        result.updateName(partnerDTO.getName());
        result = partnerDAOManager.update(result);

        if (partnerDTO.getIsExternal() != null && partnerDTO.getIsExternal()) {
            String nameLocation = partnerDTO.getNameLocation();
            if (result.getExternalType() == null) {
                ExternalTypeName externalTypeName = getExternalTypeByName(nameLocation);
                ExternalType externalType = new ExternalType(result.getId(), externalTypeName);
                result.setExternalType(externalType);
            } else {
                if (!result.getExternalType().getExternalTypeName().getName().equals(nameLocation)) {
                    ExternalTypeName externalTypeName = new ExternalTypeName(nameLocation);
                    externalTypeName = externalTypeNameDAOManager.create(externalTypeName);
                    result.getExternalType().setExternalTypeName(externalTypeName);
                }
            }
        } else {
            if (result.getExternalType() != null) {
                result.setExternalType(null);
            }
        }

        return serializer.getJSON(PartnerDTO.getPartnerDTO(result));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ExternalTypeName getExternalTypeByName(String name) {
        ExternalTypeName externalTypeName;
        try {
            externalTypeName = externalTypeNameDAOManager.findByString("name", name);
        } catch (NoResultException e) {
            externalTypeName = new ExternalTypeName(name);
            externalTypeName = externalTypeNameDAOManager.create(externalTypeName);
            //todo flush??? ExternalType сохраняется раньше чем ExternalTypeName-> у ExternalTypeName нет id -> все падает (временное решение)
            //externalTypeNameDAOManager.flush();
        }
        return externalTypeName;
    }
}
