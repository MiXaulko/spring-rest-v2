package com.businesses.service;

public interface AbstractService<T, K> {
    String create(T object);

    String get();

    void delete(K key);

    String update(T object);
}
