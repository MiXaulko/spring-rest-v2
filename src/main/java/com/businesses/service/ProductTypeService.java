package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.dto.DictionaryDTO;
import com.businesses.entity.ProductType;
import com.businesses.exceptions.EntityNotExistsException;
import com.businesses.utils.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductTypeService implements AbstractService<ProductType, Integer> {

    private DAOManager<ProductType, Integer> daoManager;

    private SerializerAdapter serializer;

    @Autowired
    public ProductTypeService(@Qualifier(value = "productTypeDAOManager") DAOManager<ProductType, Integer> daoManager,
                              SerializerAdapter serializer) {
        this.daoManager = daoManager;
        this.serializer = serializer;
    }

    @Transactional
    @CacheEvict(value = "productTypeCache", allEntries = true)
    public String create(ProductType productType) {
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.create(productType)));
    }

    @Cacheable(value = "productTypeCache")
    public String get() {
        return serializer.getJSON(DictionaryDTO.getListDictionaryDTO(daoManager.getAll()));
    }

    @Transactional
    @CacheEvict(value = "productTypeCache", allEntries = true)
    public void delete(Integer id) {
        ProductType found = daoManager.get(id);
        Preconditions.checkNotNull(found);
        daoManager.delete(found);
    }

    @Transactional
    @CacheEvict(value = "productTypeCache", allEntries = true)
    public String update(ProductType productType) {
        ProductType found = daoManager.get(productType.getId());
        Preconditions.checkNotNull(found);
        found.updateName(productType.getName());
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.update(found)));
    }
}
