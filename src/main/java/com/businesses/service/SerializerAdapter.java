package com.businesses.service;

import com.businesses.dto.DTO;
import com.businesses.dto.SimpleMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.Collection;

@Component
public class SerializerAdapter {

    private ObjectMapper mapper;

    @Autowired
    public SerializerAdapter(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public String getJSON(String message) {
        try {
            return mapper.writeValueAsString(new SimpleMessage(message));
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Не удалось преобразовать в JSON");
        }

    }

    public String getJSON(Collection<? extends DTO> dto) {
        try {
            return mapper.writeValueAsString(dto);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Не удалось преобразовать в JSON");
        }
    }

    public String getJSON(DTO dto) {
        try {
            return mapper.writeValueAsString(dto);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Не удалось преобразовать в JSON");
        }
    }
}
