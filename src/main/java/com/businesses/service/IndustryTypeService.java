package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.dto.DictionaryDTO;
import com.businesses.entity.IndustryType;
import com.businesses.exceptions.EntityNotExistsException;
import com.businesses.utils.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IndustryTypeService implements AbstractService<IndustryType, Integer> {

    private DAOManager<IndustryType, Integer> daoManager;

    private SerializerAdapter serializer;

    @Autowired
    public IndustryTypeService(@Qualifier(value = "industryTypeDAOManager") DAOManager<IndustryType, Integer> daoManager,
                               SerializerAdapter serializer) {
        this.daoManager = daoManager;
        this.serializer = serializer;
    }

    @Transactional
    @CacheEvict(value = "industryTypeCache", allEntries = true)
    public String create(IndustryType industryType) {
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.create(industryType)));
    }

    @Cacheable(value = "industryTypeCache")
    public String get() {
        return serializer.getJSON(DictionaryDTO.getListDictionaryDTO(daoManager.getAll()));
    }

    @Transactional
    @CacheEvict(value = "industryTypeCache", allEntries = true)
    public void delete(Integer id) {
        IndustryType found = daoManager.get(id);
        Preconditions.checkNotNull(found);
        daoManager.delete(found);
    }

    @Transactional
    @CacheEvict(value = "industryTypeCache", allEntries = true)
    public String update(IndustryType industryType) {
        IndustryType found = daoManager.get(industryType.getId());
        Preconditions.checkNotNull(found);
        found.updateName(industryType.getName());
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.update(found)));
    }
}
