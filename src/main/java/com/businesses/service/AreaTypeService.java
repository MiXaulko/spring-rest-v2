package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.dto.DictionaryDTO;
import com.businesses.entity.AreaType;
import com.businesses.utils.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AreaTypeService implements AbstractService<AreaType, Integer> {

    private DAOManager<AreaType, Integer> daoManager;

    private SerializerAdapter serializer;

    @Autowired
    public AreaTypeService(@Qualifier(value = "areaTypeDAOManager") DAOManager<AreaType, Integer> daoManager,
                           SerializerAdapter serializer) {
        this.daoManager = daoManager;
        this.serializer = serializer;
    }

    @Transactional
    @CacheEvict(value = "areaTypeCache", allEntries = true)
    public String create(AreaType areaType) {
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.create(areaType)));
    }

    @Cacheable(value = "areaTypeCache")
    public String get() {
        return serializer.getJSON(DictionaryDTO.getListDictionaryDTO(daoManager.getAll()));
    }

    @Transactional
    @CacheEvict(value = "areaTypeCache", allEntries = true)
    public void delete(Integer id) {
        AreaType found = daoManager.get(id);
        Preconditions.checkNotNull(found);
        daoManager.delete(found);
    }

    @Transactional
    @CacheEvict(value = "areaTypeCache", allEntries = true)
    public String update(AreaType areaType) {
        AreaType found = daoManager.get(areaType.getId());
        Preconditions.checkNotNull(found);
        found.updateName(areaType.getName());
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.update(found)));
    }
}
