package com.businesses.service;

import com.businesses.dao.DAOManager;
import com.businesses.dto.DictionaryDTO;
import com.businesses.entity.WorkerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.businesses.utils.Preconditions.checkNotNull;

@Service
public class WorkerTypeService implements AbstractService<WorkerType, Integer> {

    private DAOManager<WorkerType, Integer> daoManager;

    private SerializerAdapter serializer;

    @Autowired
    public WorkerTypeService(@Qualifier(value = "workerTypeDAOManager") DAOManager<WorkerType, Integer> daoManager,
                             SerializerAdapter serializer) {
        this.daoManager = daoManager;
        this.serializer = serializer;
    }

    @Transactional
    @CacheEvict(value = "workerTypeCache", allEntries = true)
    public String create(WorkerType workerType) {
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.create(workerType)));
    }

    @Cacheable(value = "workerTypeCache")
    public String get() {
        return serializer.getJSON(DictionaryDTO.getListDictionaryDTO(daoManager.getAll()));
    }

    @Transactional
    @CacheEvict(value = "workerTypeCache", allEntries = true)
    public void delete(Integer id) {
        WorkerType found = daoManager.get(id);
        checkNotNull(found);
        daoManager.delete(found);

    }

    @Transactional
    @CacheEvict(value = "workerTypeCache", allEntries = true)
    public String update(WorkerType workerType) {
        WorkerType result = daoManager.get(workerType.getId());
        checkNotNull(result);
        result.updateName(workerType.getName());
        return serializer.getJSON(DictionaryDTO.getDictionaryDTO(daoManager.update(result)));
    }
}
