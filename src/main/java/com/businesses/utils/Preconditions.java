package com.businesses.utils;

import com.businesses.exceptions.EntityNotExistsException;

public class Preconditions {
    public static <T> void checkNotNull(T object) {
        if (object == null) {
            throw new EntityNotExistsException();
        }
    }
}
