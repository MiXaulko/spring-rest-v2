package com.businesses.dto;

import com.businesses.entity.CompanyArea;

public class AreaDTO implements DTO{
    private Integer id;
    private String name;
    private Integer count;

    public AreaDTO() {
    }

    //todo delete this class, and refactor WorkerDTO -> constructor with 3 params

    public AreaDTO(CompanyArea companyArea) {
        this.id=companyArea.getAreaType().getId();
        this.name=companyArea.getAreaType().getName();
        this.count=companyArea.getValue();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
