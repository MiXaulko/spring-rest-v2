package com.businesses.dto;

import com.businesses.entity.Company;

public class UpdateCompanyDTO implements DTO {

    //type = default - присылаются поля Company и 2 id (OT, IT)
    //     = provider - ids provider(partner)
    //     = consumer - ids consumer(partner),producttype
    //     = areatype - id areatype, value
    //     = workertype - id workertype, value
    //     = producttype - id,year,month,value

    private String type;

    private Integer id;

    private String name;

    private String address;

    private String phone;

    private String fax;

    private Integer ownershipTypeId;

    private Integer industryTypeId;

    private Integer partnerId;

    private Integer producttypeId;

    private Integer areatypeId;

    private Integer value;

    private Integer workertypeId;

    private Integer year;

    private Integer month;

    public UpdateCompanyDTO() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Integer getOwnershipTypeId() {
        return ownershipTypeId;
    }

    public void setOwnershipTypeId(Integer ownershipTypeId) {
        this.ownershipTypeId = ownershipTypeId;
    }

    public Integer getIndustryTypeId() {
        return industryTypeId;
    }

    public void setIndustryTypeId(Integer industryTypeId) {
        this.industryTypeId = industryTypeId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public Integer getProducttypeId() {
        return producttypeId;
    }

    public void setProducttypeId(Integer productTypeId) {
        this.producttypeId = productTypeId;
    }

    public Integer getAreatypeId() {
        return areatypeId;
    }

    public void setAreatypeId(Integer areatypeId) {
        this.areatypeId = areatypeId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getWorkertypeId() {
        return workertypeId;
    }

    public void setWorkertypeId(Integer workerTypeId) {
        this.workertypeId = workerTypeId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Company createCompany() {
        return new Company(name, address, phone, fax);
    }
}
