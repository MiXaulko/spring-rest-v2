package com.businesses.dto;

import com.businesses.entity.ConsumerCompany;

public class ConsumerDTO extends PartnerDTO implements DTO {
    private Integer productId;
    private String productName;
    private Integer idFake;

    public ConsumerDTO() {
    }

    public ConsumerDTO(ConsumerCompany consumerCompany) {
        super(consumerCompany.getConsumer().getId(), consumerCompany.getConsumer().getName(),
                consumerCompany.getConsumer().getExternalType() != null,
                consumerCompany.getConsumer().getExternalType() != null ? consumerCompany.getConsumer().getExternalType().getExternalTypeName().getName() : null);
        productId=consumerCompany.getProductType().getId();
        productName=consumerCompany.getProductType().getName();
        idFake=consumerCompany.getId();
    }


    public Integer getIdFake() {
        return idFake;
    }

    public void setIdFake(Integer idFake) {
        this.idFake = idFake;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
