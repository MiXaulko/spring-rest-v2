package com.businesses.dto;

import com.businesses.entity.CompanyProductType;

public class ProductDTO implements DTO {
    private Integer id;
    private String name;
    private Integer month;
    private Integer year;
    private Integer count;
    private Integer productTypeId;

    public ProductDTO(CompanyProductType companyProductType) {
        this.id = companyProductType.getId();
        this.name = companyProductType.getProductType().getName();
        this.month = companyProductType.getMonthR();
        this.year = companyProductType.getYearR();
        this.count = companyProductType.getValue();
        this.productTypeId = companyProductType.getProductType().getId();
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
