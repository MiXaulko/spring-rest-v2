package com.businesses.dto;

import com.businesses.entity.CompanyWorker;

public class WorkerDTO implements DTO{
    private Integer id;
    private String name;
    private Integer count;

    public WorkerDTO() {
    }

    public WorkerDTO(CompanyWorker companyWorker) {
        this.id=companyWorker.getWorkerType().getId();
        this.name=companyWorker.getWorkerType().getName();
        this.count=companyWorker.getValue();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
