package com.businesses.dto;

import com.businesses.entity.Dictionary;

import java.util.List;
import java.util.stream.Collectors;

public class DictionaryDTO implements Dictionary, DTO {
    private Integer id;
    private String name;

    public DictionaryDTO(Dictionary dictionary) {
        id = dictionary.getId();
        name = dictionary.getName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static DictionaryDTO getDictionaryDTO(Dictionary dictionary) {
        return new DictionaryDTO(dictionary);
    }

    public static List<DictionaryDTO> getListDictionaryDTO(List<? extends Dictionary> collection) {
        return collection.stream().map(DictionaryDTO::new).collect(Collectors.toList());
    }
}
