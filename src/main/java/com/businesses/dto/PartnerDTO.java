package com.businesses.dto;

import com.businesses.entity.Partner;

import java.util.List;
import java.util.stream.Collectors;

public class PartnerDTO implements DTO {
    private Integer id;
    private String name;
    private Boolean isExternal;//can delete, partner is external, if contains nameLocation field
    private String nameLocation;

    public PartnerDTO() {
    }

    public PartnerDTO(Integer id, String name, Boolean isExternal, String nameLocation) {
        this.id = id;
        this.name = name;
        this.isExternal = isExternal;
        this.nameLocation = nameLocation;
    }

    public PartnerDTO(Partner partner) {
        id = partner.getId();
        name = partner.getName();
        if (partner.getExternalType() != null) {
            isExternal = true;
            nameLocation = partner.getExternalType().getExternalTypeName().getName();
        }
    }

    public static List<PartnerDTO> getListPartnerDTO(List<Partner> list) {
        return list.stream().map(PartnerDTO::new).collect(Collectors.toList());
    }

    public static PartnerDTO getPartnerDTO(Partner result) {
        return new PartnerDTO(result);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsExternal() {
        return isExternal;
    }

    public void setExternal(Boolean external) {
        isExternal = external;
    }

    public String getNameLocation() {
        return nameLocation;
    }

    public void setNameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
