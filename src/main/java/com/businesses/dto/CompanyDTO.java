package com.businesses.dto;

import com.businesses.entity.Company;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CompanyDTO implements DTO {
    private Integer id;

    private String name;

    private String address;

    private String phone;

    private String fax;

    private Set<WorkerDTO> workers = new HashSet<>();

    private Set<AreaDTO> areaTypes = new HashSet<>();

    private String ownershipType;

    private String industryType;

    private Set<ConsumerDTO> consumers = new HashSet<>();

    private Set<PartnerDTO> providers = new HashSet<>();

    private Set<ProductDTO> products = new HashSet<>();

    public CompanyDTO() {
    }

    public static List<CompanyDTO> getListCompanyDTO(Collection<Company> collection){
        return collection.stream().map(CompanyDTO::new).collect(Collectors.toList());
    }

    public CompanyDTO(Company company) {
        id = company.getId();
        name = company.getName();
        address = company.getAddress();
        phone = company.getPhone();
        fax = company.getFax();
        company.getCompanyWorkerSet().forEach(companyWorker -> workers.add(new WorkerDTO(companyWorker)));
        company.getCompanyAreaSet().forEach(companyArea -> areaTypes.add(new AreaDTO(companyArea)));
        ownershipType = company.getOwnershipType().getName();
        industryType = company.getIndustryType().getName();
        company.getConsumerCompanySet().forEach(consumerCompany -> consumers.add(new ConsumerDTO(consumerCompany)));
        company.getProviderSet().forEach(provider -> providers.add(new PartnerDTO(provider)));
        company.getCompanyProductTypeSet().forEach(product -> products.add(new ProductDTO(product)));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Set<WorkerDTO> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<WorkerDTO> workers) {
        this.workers = workers;
    }

    public Set<AreaDTO> getAreaTypes() {
        return areaTypes;
    }

    public void setAreaTypes(Set<AreaDTO> areaTypes) {
        this.areaTypes = areaTypes;
    }

    public void setConsumers(Set<ConsumerDTO> consumers) {
        this.consumers = consumers;
    }

    public String getOwnershipType() {
        return ownershipType;
    }

    public void setOwnershipType(String ownershipType) {
        this.ownershipType = ownershipType;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    public Set<ConsumerDTO> getConsumers() {
        return consumers;
    }

    public Set<PartnerDTO> getProviders() {
        return providers;
    }

    public void setProviders(Set<PartnerDTO> providers) {
        this.providers = providers;
    }
}
