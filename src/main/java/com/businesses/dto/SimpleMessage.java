package com.businesses.dto;

public class SimpleMessage
{
    private String message;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public SimpleMessage(String message)
    {
        this.message=message;
    }
}
