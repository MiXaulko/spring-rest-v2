package com.businesses.controllers;

import com.businesses.dto.UpdateCompanyDTO;
import com.businesses.service.CompanyService;
import com.businesses.service.SerializerAdapter;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.businesses.service.Constants.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(value = "/rest/company")
public class CompanyController {

    private CompanyService service;

    @Autowired
    public CompanyController(CompanyService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAll() {
        return ResponseEntity.ok(service.get());
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> create(@RequestBody UpdateCompanyDTO dto) {

        return ResponseEntity.ok(service.create(dto));
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> delete(@RequestBody UpdateCompanyDTO dto) {
        service.delete(dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> update(@RequestBody UpdateCompanyDTO dto) {
        return ResponseEntity.ok(service.update(dto));
    }
}
