package com.businesses.controllers;

import com.businesses.dto.PartnerDTO;
import com.businesses.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.businesses.service.Constants.APPLICATION_JSON_UTF8_VALUE;


@RestController
@RequestMapping(value = "/rest/partner")
public class PartnerController {

    private PartnerService service;

    @Autowired
    public PartnerController(PartnerService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAll() {
        return ResponseEntity.ok(service.get());
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> create(@RequestBody PartnerDTO partner) {
        return ResponseEntity.ok(service.create(partner));
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> delete(@RequestParam(value = "id") Integer id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> update(@RequestBody PartnerDTO partner) {
        return ResponseEntity.ok(service.update(partner));
    }
}