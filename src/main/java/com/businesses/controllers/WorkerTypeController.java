package com.businesses.controllers;

import com.businesses.entity.WorkerType;
import com.businesses.service.SerializerAdapter;
import com.businesses.service.WorkerTypeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.businesses.service.Constants.APPLICATION_JSON_UTF8_VALUE;


@RestController
@RequestMapping(value = "/rest/workertype")
public class WorkerTypeController {
    private WorkerTypeService service;

    @Autowired
    public WorkerTypeController(WorkerTypeService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAll() {
        return ResponseEntity.ok(service.get());
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> create(@RequestBody WorkerType workerType) {
        return ResponseEntity.ok(service.create(workerType));
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Integer id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> update(@RequestBody WorkerType workerType) {
        return ResponseEntity.ok(service.update(workerType));
    }
}
