package com.businesses.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class DAOManager<T, K> {

    private Class<T> entityClass;

    @PersistenceContext
    private EntityManager entityManager;


    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public DAOManager(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public T create(T obj) {
        entityManager.persist(obj);
        return obj;
    }

    public T get(K key) {
        return entityManager.find(entityClass, key);
    }

    public List<T> getAll() {
        return entityManager.createQuery(String.format("select e from %s e", entityClass.getName()), entityClass).getResultList();
    }

    public List<T> getByQuery(String query) {
        return entityManager.createQuery(query, entityClass).getResultList();
    }

    public T update(T obj) {
        return entityManager.merge(obj);
    }

    public void delete(T obj) {
        entityManager.remove(obj);
    }

    public T findByString(String nameField, String value) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(entityClass);
        Root<T> from = criteria.from(entityClass);
        criteria.select(from);
        criteria.where(builder.equal(from.get(nameField), value));
        TypedQuery<T> typed = entityManager.createQuery(criteria);
        return typed.getSingleResult();
    }

    public void flush() {
        entityManager.flush();
    }
}
