package com.businesses.dao;

import com.businesses.entity.AreaType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaTypeRepository extends JpaRepository<AreaType,Integer> {
}
