package com.businesses.dao;

import com.businesses.entity.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class DAOProducer {

    @Bean(name = "areaTypeDAOManager")
    public DAOManager<AreaType, Integer> getAreaTypeManager() {
        return new DAOManager<>(AreaType.class);
    }

    @Bean(name = "companyDAOManager")
    public DAOManager<Company, Integer> getCompanyManager() {
        return new DAOManager<>(Company.class);
    }

    @Bean(name = "ownershipTypeDAOManager")
    public DAOManager<OwnershipType, Integer> getOwnershipTypeManager() {
        return new DAOManager<>(OwnershipType.class);
    }

    @Bean(name = "industryTypeDAOManager")
    public DAOManager<IndustryType, Integer> getIndustryTypeManager() {
        return new DAOManager<>(IndustryType.class);
    }

    @Bean(name = "partnerDAOManager")
    public DAOManager<Partner, Integer> getPartnerManager() {
        return new DAOManager<>(Partner.class);
    }

    @Bean(name = "workerTypeDAOManager")
    public DAOManager<WorkerType, Integer> getWorkerTypeManager() {
        return new DAOManager<>(WorkerType.class);
    }

    @Bean(name = "productTypeDAOManager")
    public DAOManager<ProductType, Integer> getProductTypeManager() {
        return new DAOManager<>(ProductType.class);
    }

    @Bean(name = "externalTypeDAOManager")
    public DAOManager<ExternalType, Integer> getExternalTypeManager() {
        return new DAOManager<>(ExternalType.class);
    }

    @Bean(name = "externalTypeNameDAOManager")
    public DAOManager<ExternalTypeName, Integer> getExternalTypeNameManager() {
        return new DAOManager<>(ExternalTypeName.class);
    }

    @Bean(name = "companyAreaDAOManager")
    public DAOManager<CompanyArea, Integer> getCompanyAreaManager() {
        return new DAOManager<>(CompanyArea.class);
    }

    @Bean(name = "companyWorkerDAOManager")
    public DAOManager<CompanyWorker, Integer> getCompanyWorkerManager() {
        return new DAOManager<>(CompanyWorker.class);
    }

    @Bean(name = "companyProductTypeDAOManager")
    public DAOManager<CompanyProductType, Integer> getCompanyProductTypeManager() {
        return new DAOManager<>(CompanyProductType.class);
    }

    @Bean(name = "consumerCompanyDAOManager")
    public DAOManager<ConsumerCompany, Integer> getConsumerCompanyManager() {
        return new DAOManager<>(ConsumerCompany.class);
    }

    @Bean(name = "userDAOManager")
    public DAOManager<UserDb,Integer> getUserManager(){
        return new DAOManager<>(UserDb.class);
    }
}
