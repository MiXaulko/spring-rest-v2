package com.businesses.dao;

import com.businesses.entity.Company;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Integer> {
    @EntityGraph(value = "withAll", type = EntityGraph.EntityGraphType.LOAD)
    List<Company> findAll();

    @EntityGraph(value = "withAll", type = EntityGraph.EntityGraphType.LOAD)
    Company getById(Integer id);
}
