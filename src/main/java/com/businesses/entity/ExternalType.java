package com.businesses.entity;
import javax.persistence.*;

@Entity
public class ExternalType {
    @Id
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "externalTypeNameId")
    private ExternalTypeName externalTypeName;

    public ExternalType() {
    }

    public ExternalType(Integer id, ExternalTypeName externalTypeName) {
        this.id = id;
        this.externalTypeName = externalTypeName;
    }

    public ExternalTypeName getExternalTypeName() {
        return externalTypeName;
    }

    public void setExternalTypeName(ExternalTypeName externalTypeName) {
        this.externalTypeName = externalTypeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
