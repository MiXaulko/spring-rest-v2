package com.businesses.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CompanyAreaPK implements Serializable {

    private Integer areaTypeId;

    private Integer companyId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyAreaPK that = (CompanyAreaPK) o;
        return Objects.equals(areaTypeId, that.areaTypeId) &&
                Objects.equals(companyId, that.companyId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(areaTypeId, companyId);
    }
}
