package com.businesses.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class OwnershipType implements Dictionary{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "ownershipType")
    private Set<Company> companySet;

    public OwnershipType() {
    }

    public OwnershipType(String name) {
        this.name = name;
    }

    public OwnershipType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Company> getCompanySet() {
        return companySet;
    }

    public void setCompanySet(Set<Company> companySet) {
        this.companySet = companySet;
    }
}
