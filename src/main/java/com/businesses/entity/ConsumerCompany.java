package com.businesses.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(ConsumerCompanyPK.class)
public class ConsumerCompany {

    /*@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)*/
    private Integer id;

    @Id
    @Column
    private Integer consumerId;

    @Id
    @Column
    private Integer companyId;

    @Id
    @Column
    private Integer productTypeId;

    @ManyToOne
    @JoinColumn(name = "consumerId", insertable = false, updatable = false)
    private Partner consumer;

    @ManyToOne
    @JoinColumn(name = "companyId", insertable = false, updatable = false)
    private Company company;

    @ManyToOne
    @JoinColumn(name = "productTypeId", insertable = false, updatable = false)
    private ProductType productType;

    public ConsumerCompany() {
    }

    public ConsumerCompany(Partner consumer, Company company, ProductType productType) {
        this.consumer = consumer;
        this.company = company;
        this.productType = productType;
        this.consumerId = consumer.getId();
        this.companyId = company.getId();
        this.productTypeId = productType.getId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Partner getConsumer() {
        return consumer;
    }

    public void setConsumer(Partner consumer) {
        this.consumer = consumer;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Integer getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(Integer consumerId) {
        this.consumerId = consumerId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsumerCompany that = (ConsumerCompany) o;
        return Objects.equals(consumer, that.consumer) &&
                Objects.equals(company, that.company) &&
                Objects.equals(productType, that.productType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(consumer, company, productType);
    }


}
