package com.businesses.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CompanyWorkerPK implements Serializable {

    private Integer workerTypeId;

    private Integer companyId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyWorkerPK that = (CompanyWorkerPK) o;
        return Objects.equals(workerTypeId, that.workerTypeId) &&
                Objects.equals(companyId, that.companyId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(workerTypeId, companyId);
    }
}
