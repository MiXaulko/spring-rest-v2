package com.businesses.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class IndustryType implements Dictionary {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "industryType")
    private Set<Company> companySet;

    public IndustryType() {
    }

    public IndustryType(String name) {
        this.name = name;
    }

    public IndustryType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Company> getCompanySet() {
        return companySet;
    }

    public void setCompanySet(Set<Company> companySet) {
        this.companySet = companySet;
    }
}
