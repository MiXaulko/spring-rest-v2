package com.businesses.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(CompanyAreaPK.class)
public class CompanyArea {
    @Id
    private Integer areaTypeId;

    @Id
    private Integer companyId;

    private Integer value;

    @ManyToOne
    @JoinColumn(name = "companyId", insertable = false, updatable = false)
    private Company company;

    @ManyToOne
    @JoinColumn(name = "areaTypeId", insertable = false, updatable = false)
    private AreaType areaType;

    public CompanyArea() {
    }

    public CompanyArea(Company company, AreaType areaType, Integer value) {
        this.value = value;
        this.company = company;
        this.areaType = areaType;
        this.areaTypeId = areaType.getId();
        this.companyId = company.getId();
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Integer getAreaTypeId() {
        return areaTypeId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setAreaTypeId(Integer areaTypeId) {
        this.areaTypeId = areaTypeId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyArea that = (CompanyArea) o;
        return Objects.equals(value, that.value) &&
                Objects.equals(company, that.company) &&
                Objects.equals(areaType, that.areaType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value, company, areaType);
    }
}
