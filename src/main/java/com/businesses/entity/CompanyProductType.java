package com.businesses.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(CompanyProductTypePK.class)
public class CompanyProductType {

    /*@Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)*/
    private Integer id;

    @Id
    private Integer productTypeId;
    @Id
    private Integer companyId;
    @Id
    @Column(name = "yearR")
    private Integer yearR;
    @Id
    @Column(name = "monthR")
    private Integer monthR;

    @Column(name = "valueR")
    private Integer value;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompanyProductType that = (CompanyProductType) o;
        return Objects.equals(productTypeId, that.productTypeId) &&
                Objects.equals(companyId, that.companyId) &&
                Objects.equals(yearR, that.yearR) &&
                Objects.equals(monthR, that.monthR) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productTypeId, companyId, yearR, monthR, value);
    }

    public CompanyProductType() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CompanyProductType(Company company, ProductType productType, Integer yearR, Integer monthR, Integer value) {
        this.yearR = yearR;
        this.monthR = monthR;
        this.value = value;
        this.productType = productType;
        this.company = company;
        this.productTypeId = productType.getId();
        this.companyId = company.getId();
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getYearR() {
        return yearR;
    }

    public void setYearR(Integer year) {
        this.yearR = year;
    }

    public Integer getMonthR() {
        return monthR;
    }

    public void setMonthR(Integer month) {
        this.monthR = month;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne
    @JoinColumn(name = "productTypeId", insertable = false, updatable = false)
    private ProductType productType;

    @ManyToOne
    @JoinColumn(name = "companyId", insertable = false, updatable = false)
    private Company company;

}
