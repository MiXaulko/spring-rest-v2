package com.businesses.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(CompanyWorkerPK.class)
public class CompanyWorker {

    @Id
    private Integer workerTypeId;

    @Id
    private Integer companyId;

    private Integer value;

    @ManyToOne
    @JoinColumn(name = "companyId", insertable = false, updatable = false)
    private Company company;

    @ManyToOne
    @JoinColumn(name = "workerTypeId", insertable = false, updatable = false)
    private WorkerType workerType;

    public CompanyWorker(Company company, WorkerType workerType, Integer value) {
        this.value = value;
        this.company = company;
        this.workerType = workerType;
        this.companyId = company.getId();
        this.workerTypeId = workerType.getId();
    }

    public CompanyWorker() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyWorker that = (CompanyWorker) o;
        return Objects.equals(value, that.value) &&
                Objects.equals(company, that.company) &&
                Objects.equals(workerType, that.workerType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value, company, workerType);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public WorkerType getWorkerType() {
        return workerType;
    }

    public void setWorkerType(WorkerType workerType) {
        this.workerType = workerType;
    }

    public Integer getWorkerTypeId() {
        return workerTypeId;
    }

    public void setWorkerTypeId(Integer workerTypeId) {
        this.workerTypeId = workerTypeId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}
