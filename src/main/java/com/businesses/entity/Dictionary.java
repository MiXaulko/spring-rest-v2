package com.businesses.entity;

import java.io.Serializable;

public interface Dictionary extends Serializable
{
    Integer getId();

    String getName();

    void setName(String name);

    default void updateName(String name){
        this.setName(name);
    }
}
