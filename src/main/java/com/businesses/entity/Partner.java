package com.businesses.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Partner implements Dictionary{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    public Partner() {
    }

    @OneToMany(mappedBy = "consumer", cascade = CascadeType.ALL)
    private Set<ConsumerCompany> consumerCompanySet = new HashSet<ConsumerCompany>();


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ProviderCompany",
            joinColumns = @JoinColumn(name = "providerId"),
            inverseJoinColumns = @JoinColumn(name = "companyId")
    )
    private Set<Company> companySet = new HashSet<Company>();

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ExternalType externalType;

    public Partner(String name) {
        this.name = name;
    }

    public Partner(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<ConsumerCompany> getConsumerCompanySet() {
        return consumerCompanySet;
    }

    public void setConsumerCompanySet(Set<ConsumerCompany> productTypeSet) {
        this.consumerCompanySet = productTypeSet;
    }

    public Set<Company> getCompanySet() {
        return companySet;
    }

    public void setCompanySet(Set<Company> companySet) {
        this.companySet = companySet;
    }

    public ExternalType getExternalType()
    {
        return externalType;
    }

    public void setExternalType(ExternalType externalType)
    {
        this.externalType = externalType;
    }
}
