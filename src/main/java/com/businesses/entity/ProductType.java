package com.businesses.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ProductType implements Dictionary{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "productType",cascade = CascadeType.ALL)
    private Set<CompanyProductType> companyProductTypes = new HashSet<CompanyProductType>();

    @OneToMany(mappedBy = "productType",cascade = CascadeType.ALL)
    private Set<ConsumerCompany> consumerCompanySet= new HashSet<>();

    public ProductType() {

    }

    public ProductType(String name) {
        this.name = name;
    }

    public ProductType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<CompanyProductType> getCompanyProductTypes() {
        return companyProductTypes;
    }

    public void setCompanyProductTypes(Set<CompanyProductType> companyProductTypes) {
        this.companyProductTypes = companyProductTypes;
    }

    public Set<ConsumerCompany> getConsumerCompanySet() {
        return consumerCompanySet;
    }

    public void setConsumerCompanySet(Set<ConsumerCompany> consumerCompanySet) {
        this.consumerCompanySet = consumerCompanySet;
    }
}
