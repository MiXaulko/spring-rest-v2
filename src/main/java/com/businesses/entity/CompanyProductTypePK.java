package com.businesses.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CompanyProductTypePK implements Serializable {

    private Integer productTypeId;

    private Integer companyId;

    @Column(columnDefinition = "SMALLINT")
    private Integer yearR;

    @Column(columnDefinition = "SMALLINT")
    private Integer monthR;

    public Integer getYearR() {
        return yearR;
    }

    public void setYearR(Integer year) {
        this.yearR = year;
    }

    public Integer getMonthR() {
        return monthR;
    }

    public void setMonthR(Integer month) {
        this.monthR = month;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyProductTypePK that = (CompanyProductTypePK) o;
        return Objects.equals(productTypeId, that.productTypeId) &&
                Objects.equals(companyId, that.companyId) &&
                Objects.equals(yearR, that.yearR) &&
                Objects.equals(monthR, that.monthR);
    }

    @Override
    public int hashCode() {

        return Objects.hash(productTypeId, companyId, yearR, monthR);
    }
}
