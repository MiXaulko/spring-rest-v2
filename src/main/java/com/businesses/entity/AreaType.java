package com.businesses.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class AreaType implements Dictionary{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator",sequenceName = "IDGenerator",allocationSize = 1)
    private Integer id;

    private String name;

    public Set<CompanyArea> getCompanyAreaSet() {
        return companyAreaSet;
    }

    public void setCompanyAreaSet(Set<CompanyArea> companyAreaSet) {
        this.companyAreaSet = companyAreaSet;
    }

    @OneToMany(mappedBy = "areaType")
    private Set<CompanyArea> companyAreaSet = new HashSet<>();

    public AreaType() {
    }

    public AreaType(String name) {
        this.name = name;
    }

    public AreaType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AreaType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
