package com.businesses.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ConsumerCompanyPK implements Serializable {

    private Integer productTypeId;

    private Integer companyId;

    private Integer consumerId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsumerCompanyPK that = (ConsumerCompanyPK) o;
        return Objects.equals(productTypeId, that.productTypeId) &&
                Objects.equals(companyId, that.companyId) &&
                Objects.equals(consumerId, that.consumerId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(productTypeId, companyId, consumerId);
    }
}
