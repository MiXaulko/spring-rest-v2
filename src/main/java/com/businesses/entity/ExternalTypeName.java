package com.businesses.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ExternalTypeName implements Dictionary
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    public ExternalTypeName()
    {
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public ExternalTypeName(Integer id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public ExternalTypeName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nameArea)
    {
        this.name = nameArea;
    }

    public Set<ExternalType> getExternalTypes()
    {
        return externalTypes;
    }

    public void setExternalTypes(Set<ExternalType> externalTypes)
    {
        this.externalTypes = externalTypes;
    }

    @OneToMany(mappedBy = "externalTypeName")
    private Set<ExternalType> externalTypes;
}
