package com.businesses.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class WorkerType implements Dictionary {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "workerType")
    private Set<CompanyWorker> companyWorkerSet = new HashSet<>();

    public WorkerType() {
    }

    public WorkerType(String name) {
        this.name = name;
    }

    public WorkerType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<CompanyWorker> getCompanyWorkerSet() {
        return companyWorkerSet;
    }

    public void setCompanyWorkerSet(Set<CompanyWorker> companyWorkerSet) {
        this.companyWorkerSet = companyWorkerSet;
    }
}
