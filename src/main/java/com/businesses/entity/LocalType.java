package com.businesses.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LocalType {
    @Id
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalType(Integer id) {
        this.id = id;
    }

    public LocalType() {
    }
}
