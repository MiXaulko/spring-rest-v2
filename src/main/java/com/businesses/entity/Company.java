package com.businesses.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NamedEntityGraph(name = "withAll",attributeNodes = {
        @NamedAttributeNode("ownershipType"),
        @NamedAttributeNode("industryType"),
        @NamedAttributeNode("providerSet"),
        @NamedAttributeNode("companyProductTypeSet"),
        @NamedAttributeNode("companyAreaSet"),
        @NamedAttributeNode("consumerCompanySet"),
        @NamedAttributeNode("companyWorkerSet")
})
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "globalGenerator")
    @SequenceGenerator(name = "globalGenerator", sequenceName = "IDGenerator", allocationSize = 1)
    private Integer id;

    private String name;

    private String address;

    private String phone;

    private String fax;

    public Company() {
    }

    public Company(String name, String address, String phone, String fax) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
    }

    public Company(String name, String address, String phone, String fax, OwnershipType ownershipType, IndustryType industryType) {
        this(name, address, phone, fax);
        this.ownershipType = ownershipType;
        this.industryType = industryType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public OwnershipType getOwnershipType() {
        return ownershipType;
    }

    public void setOwnershipType(OwnershipType ownershipType) {
        this.ownershipType = ownershipType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<CompanyWorker> getCompanyWorkerSet() {
        return companyWorkerSet;
    }

    public void setCompanyWorkerSet(Set<CompanyWorker> companyWorkerSet) {
        this.companyWorkerSet = companyWorkerSet;
    }

    public IndustryType getIndustryType() {
        return industryType;
    }

    public void setIndustryType(IndustryType industryType) {
        this.industryType = industryType;
    }

    public Set<Partner> getProviderSet() {
        return providerSet;
    }

    public void setProviderSet(Set<Partner> providerSet) {
        this.providerSet = providerSet;
    }

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL/*, fetch = FetchType.EAGER*/)
    private Set<CompanyWorker> companyWorkerSet = new HashSet<>();

    @ManyToOne//(fetch = FetchType.EAGER)
    @JoinColumn(name = "idOwnershipType")
    private OwnershipType ownershipType;

    @ManyToOne//(fetch = FetchType.EAGER)
    @JoinColumn(name = "idIndustryType")
    private IndustryType industryType;

    @ManyToMany(mappedBy = "companySet"/*, fetch = FetchType.EAGER*/)
    private Set<Partner> providerSet = new HashSet<>();

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL/*, fetch = FetchType.EAGER*/)
    private Set<CompanyProductType> companyProductTypeSet = new HashSet<>();

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL/*, fetch = FetchType.EAGER*/)
    private Set<CompanyArea> companyAreaSet = new HashSet<>();

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL/*, fetch = FetchType.EAGER*/)
    private Set<ConsumerCompany> consumerCompanySet = new HashSet<>();

    public Set<CompanyProductType> getCompanyProductTypeSet() {
        return companyProductTypeSet;
    }

    public void setCompanyProductTypeSet(Set<CompanyProductType> companyProductTypeSet) {
        this.companyProductTypeSet = companyProductTypeSet;
    }

    public Set<CompanyArea> getCompanyAreaSet() {
        return companyAreaSet;
    }


    public void setCompanyAreaSet(Set<CompanyArea> companyAreaSet) {
        this.companyAreaSet = companyAreaSet;
    }

    public Set<ConsumerCompany> getConsumerCompanySet() {
        return consumerCompanySet;
    }

    public void setConsumerCompanySet(Set<ConsumerCompany> consumerCompanySet) {
        this.consumerCompanySet = consumerCompanySet;
    }

}
