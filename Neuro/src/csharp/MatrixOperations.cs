﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterRecognition.Core
{
    public static class MatrixOperations
    {
        public static double[,] Mul(double[,] m1, double[,] m2)
        {
            if (m1.GetLength(1) != m2.GetLength(0)) throw new Exception("Размерности матриц не совместимы");
            double[,] ans = new double[m1.GetLength(0), m2.GetLength(1)];
            for (int i = 0; i < m1.GetLength(0); i++)
                for (int j = 0; j < m2.GetLength(1); j++)
                    for (int k = 0; k < m1.GetLength(1); k++)
                        ans[i, j] += m1[i, k] * m2[k, j];
            return ans;
        }
        public static double[,] Trans(double[,] m1)
        {
            double[,] ans = new double[m1.GetLength(1), m1.GetLength(0)];
            for (int i = 0; i < m1.GetLength(0); i++)
                for (int j = 0; j < m1.GetLength(1); j++)
                    ans[j, i] = m1[i, j];
            return ans;
        }
        public static double[,] Obr(double[,] m1)
        {
            return null;
        }

        public static double[,] ObrOld(double[,] hhh)//неоптимально. переделать
        { //'единичная матрица
            if (hhh.GetLength(0) != hhh.GetLength(1)) throw new Exception("Число строк в матрице не совпадает с числом столбцов");
            int h = hhh.GetLength(0);
            //    h = 3; //активаровать для проверки. результат найдите тут: http://matrixcalc.org/

            double[,] ggg = new double[h, h];// единичная матрица           
            //   Dim A_edin(k + 1, k + 1) As Double 'затем она перейдёт в обратную
            for (int i = 0; i < h;)
            {
                for (int j = 0; j < h;)
                {
                    if (i == j)
                    { ggg[i, j] = 1; }
                    else
                    { ggg[i, j] = 0; }
                    j++;
                }
                i++;
            }
            //  'единичная матрица
            //'обратная матрица
            double arg;
            int i1;
            //  Dim arg As Double
            //   Dim i1 As Integer

            for (int j = 0; j < h;)
            {
                for (int i = 0; i < h;)
                {
                    if (i == j)
                    { goto k; }
                    arg = hhh[i, j] / hhh[j, j];
                    for (i1 = 0; i1 < h;)
                    {

                        hhh[i, i1] = hhh[i, i1] - hhh[j, i1] * arg;
                        ggg[i, i1] = ggg[i, i1] - ggg[j, i1] * arg;
                        i1++;
                    }
                    k:
                    i++;
                }
                j++;
            }

            for (int j = 0; j < h;)
            {
                for (int i = 0; i < h;)
                {
                    double arg_2;
                    if (i == j)
                    {
                        arg_2 = hhh[i, j];
                        for (i1 = 0; i1 < h;)
                        {
                            hhh[i, i1] = hhh[i, i1] / arg_2;
                            ggg[i, i1] = ggg[i, i1] / arg_2;
                            i1++;
                        }


                    }
                    i++;
                }
                j++;
            }
            return ggg;
        }

        static double[,] dop(double[,] m1)
        {
            return null;
        }

        public static double Determ(double[,] m1)
        {
            if (m1.GetLength(0) != m1.GetLength(1)) throw new Exception("Число строк в матрице не совпадает с числом столбцов");
            double det = 0;
            int Rank = m1.GetLength(0);
            if (Rank == 1) det = m1[0, 0];
            if (Rank == 2) det = m1[0, 0] * m1[1, 1] - m1[0, 1] * m1[1, 0];
            if (Rank > 2)
            {
                for (int j = 0; j < m1.GetLength(1); j++)
                {
                    det += Math.Pow(-1, 0 + j) * m1[0, j] * Determ(GetMinor(m1, 0, j));
                }
            }
            return det;
        }
        static double[,] GetMinor(double[,] m1, int row, int column)
        {
            if (m1.GetLength(0) != m1.GetLength(1)) throw new Exception("Число строк в матрице не совпадает с числом столбцов");
            double[,] buf = new double[m1.GetLength(0) - 1, m1.GetLength(0) - 1];
            for (int i = 0; i < m1.GetLength(0); i++)
                for (int j = 0; j < m1.GetLength(1); j++)
                {
                    if ((i != row) || (j != column))
                    {
                        if (i > row && j < column) buf[i - 1, j] = m1[i, j];
                        if (i < row && j > column) buf[i, j - 1] = m1[i, j];
                        if (i > row && j > column) buf[i - 1, j - 1] = m1[i, j];
                        if (i < row && j < column) buf[i, j] = m1[i, j];
                    }
                }
            return buf;
        }

        public static bool[] Reverse(bool[] m1)
        {
            bool[] res = new bool[m1.Length];
            for (int i = 0; i < m1.Length; i++)
                res[i] = !m1[i];
            return res;
        }


        public static bool Compare(bool[] m1, bool[] m2)
        {
            bool result = m1.Length == m2.Length;
            int i = 0;
            while (result && i < m1.Length)
            {
                result = m1[i].Equals(m2[i]);
                i++;
            }
            return result;
        }

        public static bool[] ToMass(bool[,] X)
        {
            int n = X.GetLength(0);
            int m = X.GetLength(1);
            bool[] ans = new bool[n * m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    ans[i * m + j] = X[i, j];
            return ans;
        }

        public static bool[,] ToMatr(bool[] X, int n, int m)
        {
            bool[,] ans = new bool[n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    ans[i, j] = X[i * m + j];
            return ans;
        }
    }
}
