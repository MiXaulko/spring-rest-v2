﻿using LetterRecognition.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LetterRecognition.Algorithms
{
    public class HopfieldPsdInv : IAlgorithm
    {
        double[,] nw;

        List<ISignVector> signs;

        public string Name
        { get { return "Hopfield: Psd Inversion"; } }

        override public string ToString()
        {
            return Name;
        }

        public object Clone()
        {
            return new HopfieldPsdInv();
        }

        public void Learn(ICollection<ISignVector> Signs)// оптимизировать
        {
            signs = Signs.ToList();
            int size = (int)Math.Pow(signs.First().Size, 2);
            List<ISignVector> lsigns = signs.ToList();
            double[,] X = new double[size, lsigns.Count];
            for (int i = 0; i < lsigns.Count; i++)
            {
                if (lsigns[i].GetVector == null) throw new ArgumentNullException();
                for (int j = 0; j < size; j++)
                    X[j, i] = lsigns[i].GetVector[j] ? 1 : -1;
            }

            nw = MatrixOperations.Mul(MatrixOperations.Mul(X, MatrixOperations.ObrOld(MatrixOperations.Mul(MatrixOperations.Trans(X), X))), MatrixOperations.Trans(X));
        }

        public ISignVector Recognize(ISignVector sign)
        {
            return GetRecognSign(Recogn(sign));
        }

        bool[] Recogn(ISignVector sign)
        {
            bool[] Yprev;
            bool[] Ycur = (bool[])sign.GetVector.Clone();
            bool end = false;
            int k = 0;
            while (!end)
            {
                Yprev = (bool[])Ycur.Clone();
                Ycur = RecognizeStep(Ycur);
                end = (MatrixOperations.Compare(Ycur, Yprev) || MatrixOperations.Compare(MatrixOperations.Reverse(Ycur), Yprev) || k >= 10000);
                k++;
            }
            return Ycur;
        }

        bool[] RecognizeStep(bool[] Y)
        {
            int size = Y.Length;
            bool[] NStep = new bool[size];
            for (int i = 0; i < (size); i++)
            {
                double d = 0;
                for (int j = 0; j < size; j++)
                    d += nw[i, j] * (Y[j] ? 1 : -1);
                NStep[i] = d >= 0;//sign?
            }
            return NStep;
        }

        ISignVector GetRecognSign(bool[] m1)
        {
            ISignVector ans = signs.Find(delegate (ISignVector SV) { return MatrixOperations.Compare(SV.GetVector, m1); });
            if (ans == null)
                ans = signs.Find(delegate (ISignVector SV) { return MatrixOperations.Compare(SV.GetVector, MatrixOperations.Reverse(m1)); });
            return ans;
        }
    }
}
