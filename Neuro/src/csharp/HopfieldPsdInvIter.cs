﻿using LetterRecognition.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LetterRecognition.Algorithms
{
    public class HopfieldPsdInvIter : IAlgorithm
    {
        double[,] nw;

        List<ISignVector> signs;

        public string Name
        { get { return "Hopfield: Psd Inversion (Iterative)"; } }

        override public string ToString()
        {
            return Name;
        }

        public object Clone()
        {
            return new HopfieldPsdInvIter();
        }

        public void Learn(ICollection<ISignVector> Signs)// оптимизировать
        {
            signs = Signs.ToList();
            int size = (int)Math.Pow(signs.First().Size, 2);
            nw = new double[size, size];
            List<ISignVector> lsigns = signs.ToList();
            for (int p = 0; p < lsigns.Count; p++)
            {
                bool[] X;
                if ((X = lsigns[p].GetVector) == null) throw new ArgumentNullException();
                double n = 0;
                double[] Y;
                {
                    for (int i = 0; i < size; i++)
                        n += Math.Pow(X[i] ? 1 : -1, 2);

                    Y = new double[size];
                    for (int i = 0; i < size; i++)
                        for (int j = 0; j < size; j++)
                            Y[i] += (X[j] ? 1 : -1) * nw[i, j];

                    double k = 0;
                    for (int i = 0; i < size; i++)
                        k += Y[i] * (X[i] ? 1 : -1);

                    n = 1 / (n - k);
                }


                Y = new double[size];

                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        Y[i] += (X[j] ? 1 : -1) * nw[j, i];
                for (int i = 0; i < size; i++)
                    Y[i] -= (X[i] ? 1 : -1);

                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        if (i != j)//???
                            nw[j, i] += Y[i] * Y[j] * n;
            }
        }

        public ISignVector Recognize(ISignVector sign)
        {
            return GetRecognSign(Recogn(sign));
        }

        bool[] Recogn(ISignVector sign)
        {
            bool[] Yprev;
            bool[] Ycur = (bool[])sign.GetVector.Clone();
            bool end = false;
            int k = 0;
            while (!end)
            {
                Yprev = (bool[])Ycur.Clone();
                Ycur = RecognizeStep(Ycur);
                end = (MatrixOperations.Compare(Ycur, Yprev) || MatrixOperations.Compare(MatrixOperations.Reverse(Ycur), Yprev) || k >= 10000);
                k++;
            }
            return Ycur;
        }

        bool[] RecognizeStep(bool[] Y)
        {
            int size = Y.Length;
            bool[] NStep = new bool[size];
            for (int i = 0; i < (size); i++)
            {
                double d = 0;
                for (int j = 0; j < size; j++)
                    d += nw[i, j] * (Y[j] ? 1 : -1);
                NStep[i] = d >= 0;//sign?
            }
            return NStep;
        }

        ISignVector GetRecognSign(bool[] m1)
        {
            ISignVector ans = signs.Find(delegate (ISignVector SV) { return MatrixOperations.Compare(SV.GetVector, m1); });
            if (ans == null)
                ans = signs.Find(delegate (ISignVector SV) { return MatrixOperations.Compare(SV.GetVector, MatrixOperations.Reverse(m1)); });
            return ans;
        }
    }
}
