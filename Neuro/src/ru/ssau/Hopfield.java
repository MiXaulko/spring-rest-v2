package ru.ssau;

import java.util.Arrays;
import java.util.List;

public class Hopfield {
    double[][] nw;

    List<Model> signs;

    public void LearnHebb(List<Model> Signs) {
        signs = Signs;
        int size = signs.get(0).getVector().length;
        nw = new double[size][size];
        List<Model> lsigns = signs;
        for (int p = 0; p < lsigns.size(); p++) {

            for (int i = 0; i < lsigns.get(p).getVector().length; i++) {
                //for (int j = 0; j < lsigns.size(); j++) {
                    for (int k = 0; k < lsigns.get(p).getVector().length; k++) {
                        if (i != k)
                            nw[i][k] += (double) ((lsigns.get(p).getVector()[i] ? 1 : -1) * (lsigns.get(p).getVector()[k] ? 1 : -1));/// (size * size);
                    }
                //}
            }


        }
        for (int i = 0; i < nw.length; i++) {
            for (int j = 0; j < nw[i].length; j++) {
                nw[i][j] /= signs.size();
            }
        }
    }

    public void Learn(List<Model> Signs) {
        signs = Signs;
        int size = signs.get(0).getVector().length;
        List<Model> lsigns = signs;
        double[][] X = new double[size][lsigns.size()];
        for (int i = 0; i < lsigns.size(); i++) {
            if (lsigns.get(i).getVector() == null) throw new IllegalArgumentException();
            for (int j = 0; j < size; j++)
                X[j][i] = lsigns.get(i).getVector()[j] ? 1 : -1;
        }

        nw = MatrixOperations.Mul(MatrixOperations.Mul(X, MatrixOperations.ObrOld(MatrixOperations.Mul(MatrixOperations.Trans(X), X))), MatrixOperations.Trans(X));
    }

    public Model Recognize(Model sign) {
        return GetRecognSign(Recogn(sign));
    }

    boolean[] Recogn(Model sign) {
        boolean[] Yprev;
        boolean[] Ycur = sign.getVector();
        boolean end = false;
        int k = 0;
        while (!end) {
            Yprev = Ycur;
            Ycur = RecognizeStep(Ycur);
            end = (MatrixOperations.Compare(Ycur, Yprev) || MatrixOperations.Compare(MatrixOperations.Reverse(Ycur), Yprev) || k >= 1000);
            k++;
        }
        return Ycur;
    }

    boolean[] RecognizeStep(boolean[] Y) {
        int size = Y.length;
        boolean[] NStep = new boolean[size];
        for (int i = 0; i < (size); i++) {
            double d = 0;
            for (int j = 0; j < size; j++)
                d += nw[i][j] * (Y[j] ? 1 : -1);
            NStep[i] = d >= 0;//sign?
        }
        return NStep;
    }

    Model GetRecognSign(boolean[] m1) {
        for (Model m : signs) {
            if (Arrays.equals(m.getVector(), m1)) {
                return m;
            }
        }
        boolean[] reversed = MatrixOperations.Reverse(m1);
        for (Model m : signs) {
            if (Arrays.equals(m.getVector(), reversed)) {
                return m;
            }
        }
        return null;
    }
}
