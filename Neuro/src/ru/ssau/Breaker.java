package ru.ssau;

import java.util.Random;

public class Breaker {
   private static Random random = new Random();

    public static Model breakModel(Model model, int percent) {
        int count = model.getVector().length * percent / 100;
        int max = model.getVector().length;
        for (int i = 0; i < count; i++) {
            int index = random.nextInt(max);
            int row = index / 10;
            int col = index % 10;
            model.getMatrix()[row][col] = inverse(model.getMatrix()[row][col]);
            model.getVector()[index] = inverse(model.getVector()[index]);
        }
        return model;
    }

    private static boolean inverse(boolean b) {
        return !b;
    }

    private static int inverse(int b) {
        return b == 1 ? 0 : 1;
    }
}
