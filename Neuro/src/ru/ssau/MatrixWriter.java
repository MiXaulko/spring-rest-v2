package ru.ssau;

import java.io.FileWriter;
import java.io.IOException;

public class MatrixWriter {
    private String path;

    public void write(Model model, String path) throws IOException {
        try (FileWriter fw = new FileWriter(path);) {
            for (int i = 0; i < model.getMatrix().length; i++) {
                for (int j = 0; j < model.getMatrix()[0].length; j++) {
                    fw.write(model.getMatrix()[i][j]+" ");
                }
                fw.write('\n');
            }
            fw.flush();
        }
    }
}
