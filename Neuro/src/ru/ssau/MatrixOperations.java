package ru.ssau;

import java.util.Arrays;

public class MatrixOperations {
    public static double[][] Mul(double[][] m1, double[][] m2) {
        if (m1[0].length != m2.length) throw new RuntimeException("Размерности матриц не совместимы");
        double[][] ans = new double[m1.length][m2[0].length];
        for (int i = 0; i < m1.length; i++)
            for (int j = 0; j < m2[0].length; j++)
                for (int k = 0; k < m1[0].length; k++)
                    ans[i][j] += m1[i][k] * m2[k][j];
        return ans;
    }

    public static double[][] Trans(double[][] m1) {
        double[][] ans = new double[m1[0].length][m1.length];
        for (int i = 0; i < m1.length; i++)
            for (int j = 0; j < m1[0].length; j++)
                ans[j][i] = m1[i][j];
        return ans;
    }

    public static double[][] ObrOld(double[][] hhh) {
        if (hhh.length != hhh[0].length)
            throw new RuntimeException("Число строк в матрице не совпадает с числом столбцов");
        int h = hhh.length;
        //    h = 3; //активаровать для проверки. результат найдите тут: http://matrixcalc.org/

        double[][] ggg = new double[h][h];// единичная матрица
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < h; j++) {
                if (i == j) {
                    ggg[i][j] = 1;
                } else {
                    ggg[i][j] = 0;
                }
            }
        }
        double arg;
        double arg_2;

        for (int j = 0; j < h; j++) {
            for (int i = 0; i < h; i++) {
                if (i != j) {
                    arg = hhh[i][j] / hhh[j][j];
                    for (int i1 = 0; i1 < h; i1++) {
                        hhh[i][i1] = hhh[i][i1] - hhh[j][i1] * arg;
                        ggg[i][i1] = ggg[i][i1] - ggg[j][i1] * arg;
                    }
                } else {
                    arg_2 = hhh[i][j];
                    for (int i1 = 0; i1 < h; ) {
                        hhh[i][i1] = hhh[i][i1] / arg_2;
                        ggg[i][i1] = ggg[i][i1] / arg_2;
                        i1++;
                    }
                }
            }
        }
        return ggg;
    }


    public static double Determ(double[][] m1) {
        if (m1.length != m1[0].length)
            throw new RuntimeException("Число строк в матрице не совпадает с числом столбцов");
        double det = 0;
        int Rank = m1.length;
        if (Rank == 1) det = m1[0][0];
        if (Rank == 2) det = m1[0][0] * m1[1][1] - m1[0][1] * m1[1][0];
        if (Rank > 2) {
            for (int j = 0; j < m1[0].length; j++) {
                det += Math.pow(-1, j) * m1[0][j] * Determ(GetMinor(m1, 0, j));
            }
        }
        return det;
    }

    static double[][] GetMinor(double[][] m1, int row, int column) {
        if (m1.length != m1[0].length)
            throw new RuntimeException("Число строк в матрице не совпадает с числом столбцов");
        double[][] buf = new double[m1.length - 1][m1.length - 1];
        for (int i = 0; i < m1.length; i++)
            for (int j = 0; j < m1[0].length; j++) {
                if ((i != row) || (j != column)) {
                    if (i > row && j < column) buf[i - 1][j] = m1[i][j];
                    if (i < row && j > column) buf[i][j - 1] = m1[i][j];
                    if (i > row && j > column) buf[i - 1][j - 1] = m1[i][j];
                    if (i < row && j < column) buf[i][j] = m1[i][j];
                }
            }
        return buf;
    }

    public static boolean[] Reverse(boolean[] m1) {
        boolean[] res = new boolean[m1.length];
        for (int i = 0; i < m1.length; i++)
            res[i] = !m1[i];
        return res;
    }


    public static boolean Compare(boolean[] m1, boolean[] m2) {
        return Arrays.equals(m1, m2);
    }

    public static boolean[] ToMass(boolean[][] X) {
        int n = X.length;
        int m = X[0].length;
        boolean[] ans = new boolean[n * m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                ans[i * m + j] = X[i][j];
        return ans;
    }

    public static boolean[][] ToMatr(boolean[] X, int n, int m) {
        boolean[][] ans = new boolean[n][m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                ans[i][j] = X[i * m + j];
        return ans;
    }
}
