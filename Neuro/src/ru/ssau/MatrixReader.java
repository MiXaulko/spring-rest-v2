package ru.ssau;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class MatrixReader {
    public Model read(String path) throws IOException {
        Model model = new Model();
        model.setC(path.charAt(path.length()-5));
        try (FileReader fw = new FileReader(path);
             Scanner br = new Scanner(fw)) {
            for (int i = 0; i < model.getMatrix().length; i++) {
                for (int j = 0; j < model.getMatrix()[0].length; j++) {
                    model.getMatrix()[i][j] = br.nextInt();
                }
            }
        }
        model.fillVector();

        return model;
    }
}
