package ru.ssau;


public class Model implements Cloneable {
    private char c;

    private int[][] matrix = new int[10][10];

    private boolean[] vector = new boolean[100];

    public char getC() {
        return c;
    }

    public void setC(char c) {
        this.c = c;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
        fillVector();
    }

    public void fillVector() {
        int k = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                vector[k++] = matrix[i][j] == 1;
            }
        }
    }

    public boolean[] getVector() {
        return vector;
    }

    public void setVector(boolean[] vector) {
        this.vector = vector;
    }

    @Override
    protected Object clone() {
        Model m = new Model();
        int[][] matr = new int[10][10];
        for (int i = 0; i < matr.length; i++) {
            System.arraycopy(matrix[i], 0, matr[i], 0, matr[0].length);
        }
        m.setMatrix(matr);
        m.setC(c);
        return m;
    }


    public void print() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Это цифра " + c);
        System.out.println("====================");
    }
}
