package ru.ssau;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        MatrixReader mr = new MatrixReader();
        String basePath = System.getProperty("user.dir") + "/Neuro/src/etalons/";
        List<String> etalonsNames = Files.walk(Paths.get(basePath))
                .filter(Files::isRegularFile)
                .map(e -> e.getFileName().toString())
                .collect(Collectors.toList());
        List<Model> etalons = new ArrayList<>();
        for (String et : etalonsNames) {
            Model m3 = mr.read(basePath + et);
            etalons.add(m3);
        }
        Hopfield h = new Hopfield();
        h.LearnHebb(etalons);
        //h.Learn(etalons);
        System.out.println("Число до преобразования");
        Model source = etalons.get(1);
        //source.print();
        int trueA = 0;
        int falseA = 0;
        for (int i = 0; i < 100; i++) {
            Model broken = Breaker.breakModel((Model) source.clone(), 0);
            //broken.print();
            Model m3 = h.Recognize(broken);
            if ((m3 != null)&&(m3.getC()==broken.getC()))
                trueA++;
                //System.out.println("Распознали цифру: " + m3.getC());
            else {
                //System.out.println("Не удалось распознать");
                falseA++;
            }
        }
        System.out.println("Удачно "+trueA+"\nНеудачно "+falseA);
    }
}
